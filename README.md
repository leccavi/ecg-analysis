# Analysis of electrocardiographic signals (ECG)

In this project a comparative study is carried out between the registered ECGs of normal patients and 
patient suffering from atrial fibrillation. To do this, four main parameters are calculated to see
if both groups can be discriminated by these parameters (which are explained in the code).

The data come from the [PTBDB](https://physionet.org/content/ptbdb/1.0.0/) (12 derivations) and [IAFDB](https://www.physionet.org/content/iafdb/1.0.0/) (II, AVF and V1 derivation) databases available at Physionet. To simplify the analysis only the second derivation will be analyzed, in this way you are being provided with an example to analyze other derivations.

[ECG_analysis.m](https://bitbucket.org/leccavi/ECG-analysis/src/master/ECG_analysis.m) and [pwaves.m](https://bitbucket.org/leccavi/ECG-analysis/src/master/pwaves.m) have been fully programmed, except for the Pan-Tompkin algorithm ([pan_tompkin.m](https://bitbucket.org/leccavi/ECG-analysis/src/master/pan_tompkin.m); Author: Hooman Sedghamiz) that was provided for R-peak wave detection. Within the codes are explained the parameters obtained, which were explained in detail in seminars on subject of biomedical signal analysis of my bachelor's degree. 

The results are in the folder called [_figures_](https://bitbucket.org/leccavi/ECG-analysis/src/master/figures/), while the data used in the folder called [_datasets_](https://bitbucket.org/leccavi/ECG-analysis/src/master/datasets/) containing only the second derivation (extracted from the databases mentioned above).

Execute the entire project by running the [run.m](https://bitbucket.org/leccavi/ecg-analysis/src/master/run.m) file. Example of what you will get:

- Analyzed ECG:

![picture](figures/s0531-re.svg)

- Summary of results:

![picture](figures/Results%20of%20computed%20parameters.svg)

Please, feel free to contact me with any questions: 

lean.lecca.96@icloud.com