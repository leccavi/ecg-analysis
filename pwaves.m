%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [indR,indP,ISOamp,beat,tm,signalfilt] = pwaves(pacfile,tm,signal)

% indR: time instant in which the maximum value has been located in the
%       wave R (expressed in samples). It is obtained by implementing the
%       Pan-Tompkins algorithm.
% indP: time instant in which the maximum value has been located in the
%       wave P (expressed in samples). It is obtained in the window
%       (indR-275ms:indR-90ms) expressed in samples.
% ISOamp: amplitude of the isoelectric line of each hearbeat
% beat: Number of beats, each one identified in the range of 275 ms 
%       before to 425 ms after each beat. 
% tm:  time vector.
% signalfilt: low-pass filtered ECG signal at 35 Hz.

fm=1000;pre=275; post=425;

% low pass filter at 35 Hz
[b,a]=butter(5,35*2/fm,'low'); signal=filtfilt(b,a,signal);

% detection of the R wave
[~,R_index,~]=pan_tompkin(signal,fm,0);

% minimum time interval between P wave and R wave
lapso=0.09*fm;
pw = zeros(length(R_index), 1);
iso = zeros(length(R_index), 1);
indR = zeros(length(R_index), 1);
beat=[];

if (R_index(1)-pre+1)<1

    % the backward window of the first beat is outside the analysis window:
    if R_index(1)<lapso
        
        % does not meet the minimum time interval between the P wave and 
        % the R  wave, so that the P wave of the first beat is not 
        % detected:
        fprintf('%s: No P-wave detected in the first heartbeat.\n',pacfile);
    else
        
        % meets minimum time interval criterion:
        eval=signal(1:R_index(1)+post);
        
        % search for the P wave (maximum value):  
        [~,indice]=max(eval(1:R_index(1)-lapso));
        isoelectric=prctile(eval(1:R_index(1)-lapso),25);
        pw(1)=indice;
        iso(1)=isoelectric;
        indR(1)=R_index(1);
    end
    
else
    
    % the backward window of the first beat is inside the analysis window:
    ini=R_index(1)-pre;
    fin=R_index(1)+post;
    eval=signal(ini:fin);
    
    % search for the P wave (maximum value):      
    [~,indice]=max(eval(1:pre-lapso));
    isoelectric=prctile(eval(1:pre-lapso),25);
    pw(1)=R_index(1)-(pre-indice)-1;
    iso(1)=isoelectric;
    indR(1)=R_index(1);
    beat=eval;
end
 
for i=2:length(R_index)
    ini=R_index(i)-pre;
    if R_index(i)+post>length(signal)
        fin=length(signal);
    else 
        fin=R_index(i)+post;
    end    
    eval=signal(ini:fin);
    
    % only include it in the study of the average beat if the 
    % [R_index-pre, R_index+post] window is inside the analysis window:
    if length(eval)==pre+post+1
        beat=[beat eval];
    end

    [~,indice]=max(eval(1:pre-lapso));
    isoelectric=prctile(eval(1:pre-lapso),25);
    pw(i)=R_index(i)-(pre-indice)-1;
    iso(i)=isoelectric;
    indR(i)=R_index(i);
end

% cleaning the first zero if the first heartbeat has not been detected:
pw = nonzeros(pw); iso = nonzeros(iso); indR = nonzeros(indR);

pamp=signal(pw)-iso;

% delete values that highly differ from the median
pos = find(pamp>=0.3*median(pamp));

% OUTPUT
indP=pw(pos);
indR=indR(pos);
ISOamp=iso(pos);
signalfilt=signal;





