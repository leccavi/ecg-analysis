%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function drawBoxplot(data, labels, glabels, ttl)

% last column of array DATA must be categorical

fig = figure('Renderer', 'painters', 'Position', [10 10 1000 700]);

for i=1:length(labels)
    subplot(ceil(sqrt(length(labels))),ceil(sqrt(length(labels))),i)
    boxplot(data(:,i),data(:,end),'notch','off','labels', glabels)
    ylabel (labels(i)), set(gca,'box','off','FontSize',18);
end

h = suptitle(ttl); set(h,'FontSize',18);

saveas(fig,fullfile([pwd '/figures/'],ttl),'svg');

end
