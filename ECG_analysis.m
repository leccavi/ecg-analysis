%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [param] = ECG_analysis(pacfile)
 
% Input: name of the .mat file containing three main variables:
% 1 - signal
% 2 - time
% 3 - sample frequency
% -------------------------------------------------------------------------
% Output: vector containing the 4 parameters to be calculated:
% - VAR (R-R): variability of the R-R interval
% - RATIO (P-R): ratio of the mean amplitude of wave P to the mean 
%                amplitude of wave R
% - VAR(t(Ppeak-R)): variability of the time interval between the maximum
%                    amplitude of the P wave and the R wave
% - RATIO2 (P-R): ratio of the maximum amplitude of the P wave to the mean
%                 amplitud of wave R substracting in both terms the 
%                 isoelectric baseline
 
% loading data:
data = load(pacfile);
sig = data.sig;
tm = data.tm;

% PWAVES function:
[indR,indP,ISOamp,beat,tm,signalfilt] = pwaves(pacfile,tm,sig);
 
%% Parameter computing

% sample frequency:
if isempty(data.fs)
    Fs = 1000;
else
    Fs = data.fs(1);
end

% number of peaks detected:
N=size(beat,2);

% R-R intervals
diffRR=diff(indR)/Fs;

% variability of the R-R interval
VARrr= 100*(std(diffRR))/mean(diffRR);

% amplitude of the R-wave of each heartbeat: 
Ramp=abs(sig(indR)-ISOamp);
RampMedia =(1/N)*sum(Ramp);
 
% amplitude of wave P of each beat (Pamp(n))
Pamp=abs(sig(indP)-ISOamp);
PampMedia=(1/N)*sum(Pamp);
 
% ratio of the mean amplitude of wave P to the mean amplitude of wave R 
% (RATIOrr):
RATIOpr=(PampMedia/RampMedia);

% variability of the time interval between the maximum amplitude of the 
% P-wave and the R-wave:
Tpeakr=(indR-indP)/Fs;
VARtPeakR=100*(std(Tpeakr)/mean(Tpeakr));
 
% average heartbeat
PromBEAT=mean(beat');
ondaRPromBEAT=find(PromBEAT==max(PromBEAT));
 
if ondaRPromBEAT <= 275
    pre=1;
    post=ondaRPromBEAT-90;
else
    pre=ondaRPromBEAT-275;
    post=ondaRPromBEAT-90;
end

% analysis window:
ondaP=PromBEAT(pre:post);
 
% amplitude of isoelectric line:
ISOampbeat=prctile(ondaP,25);
 
% amplitude of the wave P in the average beat. Maximum value of the
% analysis window in the average beat:
Pampbeat=max(ondaP);
 
%amplitud de la onda R en el latido promedio
Rampbeat=max(PromBEAT);
 
% RATIO2pr
RATIO2pr=(Pampbeat-ISOampbeat)/(Rampbeat-ISOampbeat);
 
% vector with the computed parameters
param=[VARrr, RATIOpr, VARtPeakR, RATIO2pr];
 
%% plotting results
fig = figure('Renderer', 'painters', 'Position', [10 10 1000 600]);

plot(tm,signalfilt);
hold on;
plot(tm(indP),signalfilt(indP),'ro');
plot(tm(indR),signalfilt(indR),'r*');

xlabel('Time [s]'), ylabel('Lead II (mV)');
leg = legend('ECG signal','P-peak', 'R-peak','Location','northoutside',...
    'Orientation','horizontal');legend boxoff;

pacfile(find(pacfile == '.',1,'last'):end) = [];
title(leg,sprintf('Signal: %s', pacfile));

h=gca; h.YAxis.TickLength = [0 0]; set(h,'box','off','FontSize',18);

h.XTick(end) = tm(end);
h.XTickLabel(end) = {round(tm(end))};

xlim([tm(1) tm(end)]);

saveas(fig,fullfile([pwd '/figures/'], pacfile),'svg');
end
