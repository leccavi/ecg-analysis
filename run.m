%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath('datasets'))

%% control group (CG) results:
cg = dir(fullfile([pwd '/datasets/'], '*re.mat'));

param_cg = zeros(length(cg),4);

fprintf('\n')
for i=1:length(cg)
    param_cg(i,:) = ECG_analysis(cg(i).name);
end

% since there are 2 groups in the study, I create a binary label to
% distinguish them in the whole array of results:
group = zeros(length(cg),1);

%% atrial fibrilation group (AFG):

afg = dir(fullfile([pwd '/datasets/'], 'ia*'));

param_afg = zeros(length(afg),4);

for i=1:length(afg)
    param_afg(i,:) = ECG_analysis(afg(i).name);
end

group = [group; ones(length(afg),1)];

% whole results array:
data = [param_cg; param_afg]; data = [data group];

%% Storing results - uncomment if you want to store the raw-results:
% csvwrite('results.dat',data) 
% csvwrite('controlGroup.dat',param_cg)
% csvwrite('AFGroup.dat',param_afg) 

%% Boxplot
labels = {'VAR_{RR}',  'RATIO_{PR}', 'VAR-T_{Ppeak-R}',  'RATIO2_{PR}'};
g_labels = {'Control', 'FA'};
drawBoxplot(data, labels, g_labels, 'Results of computed parameters')